cmake_minimum_required(VERSION 3.11)
project(transec)

set(CMAKE_VERBOSE_MAKEFILE ON)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

set(CMAKE_C_FLAGS "-Wall")
set(CMAKE_CXX_FLAGS "-Wall")

include_directories(include)

set(CMAKE_C_FLAGS_DEBUG "-Og -g")
set(CMAKE_CXX_FLAGS_DEBUG "-Og -g")

set(CMAKE_C_FLAGS_RELEASE "-O3")
set(CMAKE_CXX_FLAGS_RELEASE  "-O3")

add_executable(transec
        main.cpp
        src/BERsimulation.cpp
        src/channelCode.cpp
        src/fft.cpp
        include/BERsimulation.hpp
        include/bit.hpp
        include/channelCode.hpp
        include/fft.h
        include/gray.hpp
        include/polar.hpp
        include/utility.hpp)
