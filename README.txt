Polar encoder and decoder
Authors: Takuya AOKI *1, Tadashi EBIHARA *2
Organization: *1 *2 University of Tsukuba, Japan
Contact: *1 aoki@aclab.esys.tsukuba.ac.jp, *2 ebihara@iit.tsukuba.ac.jp


This is an implementation of polar code for competition "Best Channel Codes for
Short Underwater Messages".


Contents:
src/channelCode.cpp:
    A main file of the encoder and the decoder.
include/bit.hpp:
    Partial implementations of C++20 features defined in header <bit>.
include/gray.hpp:
    Gray code encoder and decoder.
include/polar.hpp:
    Polar code encoder and decoder.
CMakeLists.txt:
    CMake configuration file.
I_16.bin, I_256.bin, I_512.bin:
    Pre-calculated lookup tables for code length 16, 256 and 512, respectively.
SimulationResults.txt:
    Output of benchmark.

Other files are original benchmark codes left as is.


Prerequisites:
The codes should be compiled with --std=gnu++1z or --std=gnu++17. The codes are
tested with the following compilers:
- Clang 1000.10.44.4 on macOS Mojave 10.14.2
- GCC 8.2.0 on macOS Mojave 10.14.2 (Homebrew)
- GCC 7.4.0 on Debian stretch 9.6 (Linuxbrew)
- GCC 8.2.0 on Debian stretch 9.6 (Linuxbrew)

The following compilers cannot compile the codes:
- GCC 6 and its earlier versions

A lookup table named "I_$N.bin" ($N is replaced by code length N) should be placed
in working directory to run this codes. The lookup must contain a dump of
composite_channel_indices<N>[]. Each composite_channel_indices<N> has BER value and
corresponding composite channel indices order by channel capacity ascending.


Build:
(in source root directory)
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
    # Set -DCMAKE_C_COMPILER and -DCMAKE_CXX_COMPILER if you want to use a compiler
    # other than system-wide cc and c++.
$ make
$ ln -s ../I_* .


Run:
(in build directory)
$ ./transec


Acknowledgements:
This is heavily-refactored C++ port of original C program provided in [1].


References:
[1] "An Introduction to Polar Codes - Experience of Polar Codes in C Programming -," Ken-ichi IWATA,
    IEICE Fundamentals Review Vol.6, No.3, pp. 175-198, 2013. (Japanese)
    Available: J-STAGE, https://www.jstage.jst.go.jp/article/essfr/6/3/6_175/_pdf
