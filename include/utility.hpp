/* utility.hpp
 * Authors: Takuya AOKI *1, Tadashi EBIHARA *2
 * Organization: *1 *2 University of Tsukuba, Japan
 * Contact: *1 aoki@aclab.esys.tsukuba.ac.jp, *2 ebihara@iit.tsukuba.ac.jp
 */

#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <array>

/// std::array with value generation.
/// \tparam T Value type.
/// \tparam N Array size.
template<typename T, size_t N>
struct gen_array : std::array<T, N> {
    template<typename Generator>
    constexpr explicit gen_array(const Generator fn) : std::array<T, N>() {
        for(size_t i = 0; i < N; i++) {
            (*this)[i] = fn(i);
        }
    }
};

#endif //UTILITY_HPP
