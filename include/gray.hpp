/* Gray code
 * Authors: Takuya AOKI *1, Tadashi EBIHARA *2
 * Organization: *1 *2 University of Tsukuba, Japan
 * Contact: *1 aoki@aclab.esys.tsukuba.ac.jp, *2 ebihara@iit.tsukuba.ac.jp
 *
 * This function generates a lookup table that converts binary data and gray code at compile time.
 * The use of the lookup table is to avoid runtime calculation or hard-coding the table.
 */

#ifndef GRAY_H
#define GRAY_H

#include <type_traits>

#include "utility.hpp"

/// Gray code.
class gray {
public:
    /// Encode the value.
    /// \tparam N Number of bits of value.
    /// \tparam T Value type.
    /// \param value Value.
    /// \return Encoded value.
    template<size_t N, typename T,
        std::enable_if_t<std::is_unsigned<T>::value, std::nullptr_t> = nullptr,
        std::enable_if_t<N <= 8*sizeof(T), std::nullptr_t> = nullptr>
    static constexpr T encode(T value) {
        constexpr gen_array<T, size_t(1) << N> table([](size_t i) { return i ^ (i >> 1); });
        return table[value];
    }

    /// Decode the value.
    /// \tparam N Number of bits of value.
    /// \tparam T Value type.
    /// \param value Value.
    /// \return Decoded value.
    template<size_t N, typename T,
            std::enable_if_t<std::is_unsigned<T>::value, std::nullptr_t> = nullptr,
            std::enable_if_t<N <= 8*sizeof(T), std::nullptr_t> = nullptr>
    static constexpr T decode(T value) noexcept {
        constexpr gen_array<T, size_t(1) << N> table([](size_t i) {
            auto result = T{};
            for(size_t j = 0; j < N; j++) {
                const auto mask = T(1) << (N - j - 1);
                result |= ((i >> 1) & mask) ^ (i & mask);
            }

            return result;
        });

        return table[value];
    }
};

#endif //GRAY_H