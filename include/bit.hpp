/* bit.hpp
 * Authors: Takuya AOKI *1, Tadashi EBIHARA *2
 * Organization: *1 *2 University of Tsukuba, Japan
 * Contact: *1 aoki@aclab.esys.tsukuba.ac.jp, *2 ebihara@iit.tsukuba.ac.jp
 *
 * This file contains partial implementations of C++20 features defined in header <bit>.
 */

#ifndef BIT_HPP
#define BIT_HPP

#include <type_traits>

template<typename T, std::enable_if_t<std::is_unsigned<T>::value, std::nullptr_t> = nullptr>
constexpr bool ispow2(T value) noexcept {
    return value != 0 && (value & (value - 1)) == 0;
}

template<typename T, std::enable_if_t<std::is_unsigned<T>::value, std::nullptr_t> = nullptr>
constexpr T log2p1(T x) noexcept {
    return x == 0 ? 0 : (1 + log2p1(x >> 1));
}

#endif //BIT_HPP