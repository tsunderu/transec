/* Polar code
 * Authors: Takuya AOKI *1, Tadashi EBIHARA *2,
 *          Ken-ichi IWATA (original), Yusuke SUSUKI (original), Shinya OZAWA (original)
 * Organization: *1 *2 University of Tsukuba, Japan
 * Contact: *1 aoki@aclab.esys.tsukuba.ac.jp, *2 ebihara@iit.tsukuba.ac.jp
 *
 * This file performs Polar encoding and decoding. A lookup table named "I_$N.bin" ($N is replaced by
 * code length N) should be placed in working directory to run this file. The lookup must contain a
 * dump of composite_channel_indices<N>[]. Each composite_channel_indices<N> has BER value and
 * corresponding composite channel indices order by channel capacity ascending.
 *
 * This file is heavily-refactored C++ port of original C program provided in [1].
 *
 * [1] "An Introduction to Polar Codes - Experience of Polar Codes in C Programming -," Ken-ichi IWATA,
 *     IEICE Fundamentals Review Vol.6, No.3, pp. 175-198, 2013. (Japanese)
 *     Available: J-STAGE, https://www.jstage.jst.go.jp/article/essfr/6/3/6_175/_pdf
 */

#ifndef POLAR_H
#define POLAR_H

#include <fstream>
#include <string>
#include <algorithm>
#include <bitset>
#include <vector>
#include <cstdlib>
#include <cassert>

#include "bit.hpp"

/// 2-dimensional array.
/// \tparam T Value type.
/// \tparam NY Size of second dimension of the array.
/// \tparam NX Size of first dimension of the array.
template<typename T, size_t NY, size_t NX>
class array_2d
{
    T* const data;

public:
    /// Instantiate new array without filling the backing array.
    array_2d() : data(new T[NY*NX]) {}

    /// Instantiate new array. The backing array is filled with the specified value.
    /// \param value Value with which the backing array is filled.
    explicit array_2d(const T& value) : data(new T[NY*NX]) {
        std::fill_n(data, NY*NX, value);
    }

    ~array_2d() { delete[] data; }

    /// Access the array element.
    /// \param j Index of second dimension.
    /// \param i Index of first dimension.
    /// \return Reference to the element of specified position.
    T& operator()(size_t j, size_t i) const noexcept {
        return data[NX*j + i];
    }
};

/// Represents fractional value.
struct fraction {
    /// Numerator.
    double num;

    /// Denominator.
    double den;
};

template <size_t N>
struct composite_channel_indices {
    /// BER
    double ber;

    /// Composite channel indices order by channel capacity ascending.
    uint16_t indices[N];
};

/// Coder and decoder of polar codes.
/// \tparam N Code length. N must be power-of-2 value.
/// \tparam K Number of information bits in code block. K must be less than or equal to N.
template<size_t N, size_t K>
class polar_code {
    static_assert(ispow2(N));
    static_assert(K <= N);

private:
    /// Exponent M such that N = pow(2, M)
    static constexpr auto M = log2p1(N) - 1;

    /// Pairs of BER and composite channel indices ordered by channel capacity ascending.
    std::vector<composite_channel_indices<N>> i_table;

    /// Memo type for recursive L() calculation.
    using l_memo = array_2d<fraction, N, M + 1>;

    /// Reverse the bit order of value.
    /// \param value Value to be reversed.
    /// \return Reversed value.
    static uint32_t reverse_bits(uint32_t value) noexcept {
        value = (value & 0x55555555u) << 1 | (value & ~0x55555555u) >> 1;
        value = (value & 0x33333333u) << 2 | (value & ~0x33333333u) >> 2;
        value = (value & 0x0f0f0f0fu) << 4 | (value & ~0x0f0f0f0fu) >> 4;
        value = (value & 0x00ff00ffu) << 8 | (value & ~0x00ff00ffu) >> 8;
        value = (value & 0x0000ffffu) << 16 | (value & ~0x0000ffffu) >> 16;
        return value >> (32 - M);
    }

    /// Transform vector s by applying M-th Kronecker power of transform matrix T = {{1, 0}, {1, 1}}.
    /// \param s Bit vector of N-bit length.
    ///
    /// See Eq. (10) and Fig. 8 in [1]. <br />
    /// [1] <a href="https://www.jstage.jst.go.jp/article/essfr/6/3/6_175/_pdf">"An Introduction to Polar Codes
    /// - Experience of Polar Codes in C Programming -," Ken-ichi IWATA, IEICE Fundamentals Review Vol.6, No.3, pp. 175-198, 2013. (Japanese)</a>
    static void transform(std::vector<uint8_t>& s) noexcept {
        for (size_t i = 1; i <= M; i++) {
            for (size_t j = 0; j < N; j++) {
                if ((j % (1 << i)) < (1u << (i - 1))) {
                    s[j] = s[j] ^ s[j + (1u << (i - 1))];
                }
            }
        }
    }

    /// Calculate likelihood for estimation of information bit.
    /// \param i
    /// \param j
    /// \param u2
    /// \param len
    /// \param nd Memo for recursive L() calculation
    /// \return Likelihood.
    ///
    /// See Eqs. (26)-(29) in [1]. <br />
    /// [1] <a href="https://www.jstage.jst.go.jp/article/essfr/6/3/6_175/_pdf">"An Introduction to Polar Codes
    /// - Experience of Polar Codes in C Programming -," Ken-ichi IWATA, IEICE Fundamentals Review Vol.6, No.3, pp. 175-198, 2013. (Japanese)</a>
    static fraction L(size_t i, size_t j, const std::vector<uint8_t>& u2, size_t len, l_memo& nd) {
        const size_t k = 1u << (M - j);
        const auto k2 = 2*k;
        const auto len_h = len/2;
        std::vector<uint8_t> u2_(len_h);

        if (std::isnan(nd(i, j).num)) {
            if (len_h > 0) {
                if ((i % k2) < k) {
                    for (size_t t = 0; t < len_h; t++) {
                        u2_[t] = u2[2 * t] ^ u2[2 * t + 1];
                    }
                } else {
                    for (size_t t = 0; t < len_h; t++) {
                        u2_[t] = u2[2 * t + 1];
                    }
                }
            }

            nd(i, j) = L(i, j + 1, u2_, len_h, nd);
        }

        const auto ix = i^k;
        if (std::isnan(nd(ix, j).num)) {
            if (len_h > 0) {
                if ((ix % k2) < k) {
                    for (size_t t = 0; t < len_h; t++)
                        u2_[t] = u2[2 * t] ^ u2[2 * t + 1];
                } else {
                    for (size_t t = 0; t < len_h; t++) {
                        u2_[t] = u2[2 * t + 1];
                    }
                }
            }

            nd(ix, j) = L(ix, j + 1, u2_, len_h, nd);
        }

        // Numerator of L. See Eqs. (28) and (29).
        const auto zero = (len & 1)
                    ? (u2[len - 1] ? nd(ix, j).den : nd(ix, j).num) * nd(i, j).num
                    : nd(i, j).num * nd(ix, j).num + nd(i, j).den * nd(ix, j).den;

        // Denominator of L. See Eqs. (28) and (29).
        const auto one = (len & 1)
                   ? (u2[len - 1] ? nd(ix, j).num : nd(ix, j).den) * nd(i, j).den
                   : nd(i, j).num * nd(ix, j).den + nd(i, j).den * nd(ix, j).num;

        const auto sum = zero + one;

        return fraction{zero/sum, one/sum};
    }

    /// Get information bit map.
    /// \param ber BER
    /// \return N-bit bitset indicating whether the composite channel corresponding to bitset's index
    /// is for information bit.
    auto get_information_bit_map(double ber) const {
        // Obtain the item of i_table closest to the specified BER
        const auto& i_table_item = (*std::min_element(i_table.begin(), i_table.end(),
            [ber](const auto &x, const auto &y) { return std::abs(x.ber - ber) < std::abs(y.ber - ber);}));

        std::bitset<N> information_bit_map;

        // Set information bit mapping
        for (size_t i = 0; i < K; i++) {
            information_bit_map[i_table_item.indices[N - i - 1]] = true;
        }

        return information_bit_map;
    }

public:
    /// Instantiate polar_code.
    polar_code() {
        // Read indices table for code length N
        const auto filename = "I_" + std::to_string(N) + ".bin";
        std::ifstream ifs(filename, std::ios_base::in | std::ios_base::binary);

        do {
            composite_channel_indices<N> item;
            ifs.read(reinterpret_cast<char *>(&item), sizeof(decltype(item)));
            i_table.push_back(item);
        } while (!ifs.eof());

        ifs.close();
    }

    /// Encode the information bit vector.
    /// \param info_bits Information bit vector of K bits.
    /// \param ber Channel BER estimated.
    /// \return Encoded bit vector of N bits.
    auto encode(const std::vector<uint8_t>& info_bits, double ber) {
        assert(info_bits.size() == K);

        auto information_bit_map = get_information_bit_map(ber);

        // Store the information bits into input vector z.
        std::vector<uint8_t> z(N);
        size_t info_bits_index = 0;
        for (size_t i = 0; i < N; i++) {
            if (information_bit_map[i]) {
                // This bit is for information.
                z[i] = info_bits[info_bits_index];
                info_bits_index++;
            } else {
                // This bit is frozen. Frozen bit sequence can be any sequence known by the coder
                // and the decoder. In this implementation zero vector is used.
                z[i] = 0;
            }
        }

        // Transform the input vector z. See Eq. (11).
        std::vector<uint8_t> z2(N);
        for(uint32_t i = 0; i < N; i++) {
            z2[reverse_bits(i)] = z[i];
        }
        transform(z2);

        return z2;
    }

    /// Decode the code bit vector.
    /// \param code_bits Encoded bit vector of N bits.
    /// \param ber Channel BER estimated.
    /// \return Decoded bit vector of K bits.
    auto decode(const std::vector<uint8_t>& code_bits, double ber) {
        assert(code_bits.size() == N);

        auto information_bit_map = get_information_bit_map(ber);

        // Initialize memo for L() calculation.
        l_memo nd(fraction({std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()}));
        for (size_t i = 0; i < N; i++) {
            nd(i, M).num = code_bits[i] ? ber : 1 - ber; //BSC,
            nd(i, M).den = code_bits[i] ? 1 - ber : ber;  //p=W(1|0)=W(0|1)
        }

        // Estimate bits successively. See Eqs. (26).
        std::vector<uint8_t> hat_z(N);
        for (size_t i = 0; i < N; i++) {
            if (information_bit_map[i]) {
                auto ir = reverse_bits(static_cast<uint32_t>(i));
                nd(ir, 0) = L(ir, 1, hat_z, i, nd);
                hat_z[i] = uint8_t(nd(ir, 0).num < nd(ir, 0).den ? 1 : 0);
            } else {
                // Frozen bit.
                hat_z[i] = 0;
            }
        }

        // Pick up the information bits.
        std::vector<uint8_t> info_bits(K);
        size_t info_bits_index = 0;
        for(size_t i = 0; i < N; i++) {
            if(information_bit_map[i]) {
                info_bits[info_bits_index] = hat_z[i];
                info_bits_index++;
            }
        }

        return info_bits;
    }
};

#endif //POLAR_H
