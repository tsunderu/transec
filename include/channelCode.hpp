#pragma once

#include <stdint.h>
#include <vector>
#include <complex>

using symbolVector = std::vector<uint8_t>;
using complexVector = std::vector<std::complex<double> >;

class channelCode
{
public:
    void encode(symbolVector &codeSymbols, uint8_t &pskCardinality, const symbolVector &infoBits, const double EbN0);

    void decode(symbolVector &estimated, const complexVector &received, const double EbN0);

    // you might add some code here
    channelCode(void)
    {
    }
};

