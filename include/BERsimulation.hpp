#pragma once

#include "channelCode.hpp"
#include "fft.h"

#include <iostream>
#include <random>
#include <ctime>
#include <vector>
#include <stdint.h>
#include <complex>
#include <cmath>
#include <fstream>
#include <cassert>
#include <chrono>
#include <algorithm>

using symbolVector = std::vector<uint8_t>;
using complexVector = std::vector<std::complex<double> >;

class BERsimulation
{
private:
    void bitSource(symbolVector &infoBits);

    void pskMapping(complexVector &pskSymbols, const symbolVector &codeSymbols, const uint8_t &pskSize);

    void channel(complexVector &pskSymbols, std::normal_distribution<> &awgn);

    void countErrors(uint32_t &bitErrorCount, uint32_t &blockErrorCount, const symbolVector &infoBits, const symbolVector &estimated);

    void outputResults(const uint32_t bitErrorCount, const uint32_t blockErrorCount, const uint32_t loopCount, const double meanEncodeTime, const double meanDecodeTime, const double EbN0 );

    double calcRefTime(void);


public:
    void doSimulation(void);


public:
    BERsimulation(void) :
        pi(std::acos(-1) ),
        gen(2031),
        binDistr(0,1),
        uniRealDistr(0.9,1.1),
        pskSize(1),
        infoBits(numInfobits),
        codeSymbols(2*numInfobits),
        pskSymbols(2*numInfobits),
        estimated(numInfobits),
        maxEncodeTime(0),
        maxDecodeTime(0)
    {
    }

private:
    const uint32_t numInfobits = 128;
    const uint32_t maxIterations = std::pow(10,7);
    const uint32_t minIterations = std::pow(10,4);
    const uint32_t minErrors = 100;
    const double minEbN0 = -8.0;
    const double maxEbN0 = 8.0;
    const double stepEbN0 = 1.0;

    const double pi;
    std::mt19937 gen;
    std::uniform_int_distribution<> binDistr;
    std::uniform_real_distribution<> uniRealDistr;
    uint8_t pskSize;
    symbolVector infoBits;
    symbolVector codeSymbols;
    complexVector pskSymbols;
    symbolVector estimated;
    std::ofstream myfile;
    double maxEncodeTime;
    double maxDecodeTime;
};
