#pragma once

#include <stdint.h>
#include <vector>
#include <complex>


std::vector<std::complex<double>> fft(const std::vector<std::complex<double>> &timeSignal);
