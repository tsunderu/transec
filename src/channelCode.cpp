/*
 code name: Polar code (with Gray code PSK mapping)
 author: Takuya AOKI *1, Tadashi EBIHARA *2
 organization: *1 *2 University of Tsukuba, Japan
 contact: *1 aoki@aclab.esys.tsukuba.ac.jp, *2 ebihara@iit.tsukuba.ac.jp
 patents:
 remarks: See polar.hpp, gray.hpp, bit.hpp and utility.hpp for further information.
*/

#include <algorithm>

#include "channelCode.hpp"
#include "polar.hpp"
#include "gray.hpp"


// Settings

/// Number of bits in a symbol, 1 for BPSK and 2 for QPSK. 3 for 8-PSK cannot be used due to
/// the limitation of code length n.
static constexpr size_t bitsPerSymbol = 2;

/// Code length. Any power-of-2 values can be used.
static constexpr size_t n = 512;


// Constants

/// Number of information bit in a code block. Length of infoBits should be divisible by k.
static constexpr auto k = n/2/bitsPerSymbol;
// NOTE: Number of information bits is hard-coded because it can be obtained only at runtime.
static_assert(128%k == 0);

/// PSK cardinality.
static constexpr size_t cardinality = 1 << bitsPerSymbol;

/// Mathematical constant Pi.
static constexpr auto pi = 3.14159265358979323846;

/// Normalize the angle into (-pi, pi]
/// \param angle Angle in radian.
/// \return Normalized angle.
static constexpr double normalize_angle(double angle) noexcept {
    while(angle <= -pi) angle += 2*pi;
    while(angle > pi) angle -= 2*pi;
    return angle;
}

/// Angle map for PSK demodulation.
// Decode Gray-coded value here for performance. The indices of angle_map are decoded value.
static constexpr gen_array<double, cardinality> angle_map([](size_t i) {
    return normalize_angle(2*pi*gray::encode<bitsPerSymbol>(i)/cardinality);
});

static polar_code<n, k> polar;

void channelCode::encode(symbolVector &codeSymbols, uint8_t &pskCardinality, const symbolVector &infoBits, const double EbN0)
{
    pskCardinality = static_cast<uint8_t>(cardinality);

    // Calculate BER from variable EbN0. The variable EbN0 changing in benchmark seems to be Es/N0.
    // And Es/N0 is actually 3 dB worse than the value of variable EbN0 because Es/N0 noises are
    // added to the signal twice at src/BERsimulation.cpp:27.
    const auto ber = std::erfc(std::sqrt(std::pow(10.0, EbN0/10.0)/2/bitsPerSymbol))/2;

    auto oit = codeSymbols.begin();
    // Encode infoBits with polar code. k-bit information sub-block -> n-bit code sub-block
    for(auto iit = infoBits.begin(); iit != infoBits.end(); iit += k) {
        const auto subBlockCodeBits = polar.encode(symbolVector(iit, iit + k), ber);

        // Map n-bit code into psk symbols.
        for(auto cit = subBlockCodeBits.begin(); cit != subBlockCodeBits.end(); cit += bitsPerSymbol) {
            uint8_t value = 0;
            for(auto i = 0; i < bitsPerSymbol; i++) {
                value |= *(cit + i) << i;
            }

            // Apply Gray code for better BER.
            value = gray::encode<bitsPerSymbol>(value);

            *oit = value;
            oit++;
        }
    }
}

void channelCode::decode(symbolVector &estimated, const complexVector &received, const double EbN0)
{
    // Calculate BER from variable EbN0. The variable EbN0 changing in benchmark seems to be Es/N0.
    // And Es/N0 is actually 3 dB worse than the value of variable EbN0 because Es/N0 noises are
    // added to the signal twice at src/BERsimulation.cpp:27.
    const auto ber = std::erfc(std::sqrt(std::pow(10.0, EbN0/10.0)/2/bitsPerSymbol))/2;

    // Demodulate the signal.
    symbolVector codeSymbols(received.size());
    for(size_t i = 0; i < received.size(); i++) {
        auto theta = std::atan2(received[i].imag(), received[i].real());
        auto value = std::min_element(angle_map.begin(), angle_map.end(), [theta](const auto a, const auto b) {
            return std::abs(normalize_angle(a - theta)) < std::abs(normalize_angle(b - theta));
        }) - angle_map.begin();

        codeSymbols[i] = static_cast<uint8_t>(value);
    }

    // Decode codeSymbols with polar code. n-bit code sub-block -> k-bit information sub-block
    auto oit = estimated.begin();
    for(auto iit = codeSymbols.begin(); iit != codeSymbols.end(); iit += n/bitsPerSymbol, oit += k) {
        symbolVector subBlockCodeBits(n);
        auto i = 0;
        for(auto thisIit = iit; thisIit != iit + n/bitsPerSymbol; thisIit++) {
            for(auto j = 0; j < bitsPerSymbol; j++, i++) {
                subBlockCodeBits[i] = static_cast<uint8_t>((*thisIit >> j) & 1);
            }
        }

        auto estimatedSubBlock = polar.decode(subBlockCodeBits, ber);
        std::copy(estimatedSubBlock.begin(), estimatedSubBlock.end(), oit);
    }
}
