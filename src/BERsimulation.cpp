#include "BERsimulation.hpp"

// randomly fills vector infoBits with 0 and 1
void BERsimulation::bitSource(symbolVector &infoBits)
{
    for(auto &n : infoBits)
    {
        n=binDistr(gen);
    }
}

// maps codeSymbols to pskSymbols, PSK cardinality given by pskSize
void BERsimulation::pskMapping(complexVector &pskSymbols, const symbolVector &codeSymbols, const uint8_t &pskSize)
{
    std::complex<double> j(0, 1);
    for(uint32_t n=0; n < codeSymbols.size(); n++)
    {
        pskSymbols[n] = exp(j*2.0*pi*static_cast<double>(codeSymbols[n] )/static_cast<double>(pskSize));
    }
}

// channel causes small amplitude distortion and adds white Gaussian noise to pskSymbols
void BERsimulation::channel(complexVector &pskSymbols, std::normal_distribution<> &awgn)
{
    for(auto &n: pskSymbols)
    {
        n = uniRealDistr(gen)*n + std::complex<double>(awgn(gen), awgn(gen) );
    }
}

// compares infoBits and estimated, increases bitErrorCount and blockErrorCount accordingly
void BERsimulation::countErrors(uint32_t &bitErrorCount, uint32_t &blockErrorCount, const symbolVector &infoBits, const symbolVector &estimated)
{
    assert(infoBits.size() == estimated.size() );
    uint32_t errorCount = 0;
    for(uint32_t n=0; n < estimated.size(); n++)
    {
        if(infoBits[n] != estimated[n] )
        {
            errorCount++;
        }
    }
    bitErrorCount += errorCount;
    if (errorCount > 0)
    {
        blockErrorCount++;
    }
}

// calculates bit and block error rate, writes simulation results to file and console
void BERsimulation::outputResults(const uint32_t bitErrorCount, const uint32_t blockErrorCount, const uint32_t loopCount, const double meanEncodeTime, const double meanDecodeTime, const double EbN0 )
{
    double ber = static_cast<double>(bitErrorCount)/static_cast<double>(loopCount)/static_cast<double>(numInfobits);
    double bler = static_cast<double>(blockErrorCount)/static_cast<double>(loopCount);

    std::cout << "number of iterations: " << loopCount << '\n';
    std::cout << "number of bit errors: " << bitErrorCount << '\n';
    std::cout << "number of block errors: " << blockErrorCount << '\n';
    std::cout << "Bit error rate: " << ber << '\n';
    std::cout << "Block error rate: " << bler << '\n';
    std::cout << "Encoding Time: " << meanEncodeTime << "ms" << '\n';
    std::cout << "Decoding Time: " << meanDecodeTime << "ms" <<'\n' << '\n';

    myfile << EbN0 <<"," << ber << "," << bler << "," << meanEncodeTime << "," << meanDecodeTime << "," << loopCount << "," << bitErrorCount << "," << blockErrorCount << '\n';
}

// calculates reference time based on FFT computations
double BERsimulation::calcRefTime(void)
{
    const uint16_t fftSize = 512;
    const uint16_t fftIter = 1000;
    const double timeFactor = 10.0;
    double fftTime = 0;
    symbolVector ut(fftSize);
    complexVector xt(fftSize);
    std::uniform_int_distribution<> uniIntDistr (0, 7);

    for(uint16_t k=0; k < fftIter; k++)
    {
        for(uint16_t n=0; n < fftSize; n++)
        {
            ut[n] = uniIntDistr(gen);
        }
        pskMapping(xt, ut, 8);
        std::chrono::steady_clock::time_point fftStart = std::chrono::steady_clock::now();
        complexVector Xf = fft(xt);
        std::chrono::steady_clock::time_point fftEnd = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> fft_ms = fftEnd - fftStart;
        fftTime += fft_ms.count();
    }

    return timeFactor*fftTime/static_cast<double>(fftIter);
}

// simulation of whole transmission chain to get bit error rate and block error rate for Eb/N0 in range [minEbN0, maxEbN0]
// measures time of encoding and decoding, results written to "SimulationResults.txt" and console
void BERsimulation::doSimulation(void)
{
    using namespace std::chrono;

    double refTime = calcRefTime();

    myfile.open("SimulationResults.txt");
    myfile << "# reference time in ms: " << refTime << '\n';
    myfile << "# EbN0_dB,BER,BLER,meanEncodeTime,meanDecodeTime,numIterations,bitErrors,blockErrors" << '\n';

    channelCode fec;

    for(int n = 0; (minEbN0 + n*stepEbN0) <= maxEbN0; n++)
    {
        double EbN0 = minEbN0 + n*stepEbN0;
        std::cout << "EbN0 = " << EbN0 << " dB" << '\n';
        uint32_t loopCount = 0;
        uint32_t bitErrorCount = 0;
        uint32_t blockErrorCount = 0;
        double encodeTime = 0.0;
        double decodeTime = 0.0;

        // noise distribution, factor sqrt(301/300) needed because of multiplicative amplitude distortion
        std::normal_distribution<> awgn(0.0, std::pow(10.0, -EbN0/20.0) * sqrt(301.0/300.0) );

        for(loopCount = 0; (loopCount <= minIterations || blockErrorCount <= minErrors) && (loopCount <= maxIterations); loopCount++)
        {
            // generate information bits
            bitSource(infoBits);

            // encoding (with time measurement)
            steady_clock::time_point t1 = steady_clock::now();
            fec.encode(codeSymbols, pskSize, infoBits, EbN0);
            steady_clock::time_point t2 = steady_clock::now();
            duration<double, std::milli> encode_ms = t2 - t1;
            encodeTime += encode_ms.count();
            assert( pskSize > 1 && pskSize < 9 );
            assert( codeSymbols.size() <= 2*numInfobits );

            // mapping to M-PSK symbols, M=pskSize
            pskMapping(pskSymbols, codeSymbols, pskSize);

            // transmission over channel (amplitude distortion + AWGN)
            channel(pskSymbols, awgn);

            // decoding (with time measurement)
            steady_clock::time_point t3 = steady_clock::now();
            fec.decode(estimated, pskSymbols, EbN0);
            steady_clock::time_point t4 = steady_clock::now();
            duration<double, std::milli> decode_ms = t4 - t3;
            decodeTime += decode_ms.count();

            // count bit errors, if necessary, increase error counters
            countErrors(bitErrorCount, blockErrorCount, infoBits, estimated);

            if(maxIterations/20 > 0)
            {
                if(loopCount % (maxIterations/20) == 0)
                {
                    std::cout << loopCount << '\n';
                }
            }
        }

        double meanEncodeTime = encodeTime/static_cast<double>(loopCount);
        double meanDecodeTime = decodeTime/static_cast<double>(loopCount);

        maxEncodeTime = std::max(maxEncodeTime, meanEncodeTime);
        maxDecodeTime = std::max(maxDecodeTime, meanDecodeTime);

        outputResults(bitErrorCount, blockErrorCount, loopCount, meanEncodeTime, meanDecodeTime, EbN0);
    }

    // complexity estimation output
    std::cout << "time limit: " << refTime << "ms" << '\n';
    std::cout << "encoding time: " << maxEncodeTime << "ms" << '\n';
    std::cout << "decoding time: " << maxDecodeTime << "ms" << '\n';

    myfile.close();
}
