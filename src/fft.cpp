#include "fft.h"

std::vector<std::complex<double>> fft(const std::vector<std::complex<double>> &timeSignal)
{
    // TO DO: sicherstellen, dass L�nge von TimeSignal eine Potenz von 2 ist
    uint16_t N = timeSignal.size();
    std::vector<std::complex<double>> spectrum = timeSignal;
    if (N == 1)
    {
        spectrum = timeSignal;
    }
    else if (N == 2)
    {
        spectrum[0] = timeSignal[0] + timeSignal[1];
        spectrum[1] = timeSignal[0] - timeSignal[1];
    }
    else
    {
        std::vector<std::complex<double>> even(N/2);
        std::vector<std::complex<double>> odd(N/2);
        std::vector<std::complex<double>> eSpec(N/2);
        std::vector<std::complex<double>> oSpec(N/2);
        for (int n=0; n<N/2; ++n)
        {
            even[n] = timeSignal[2*n];
            odd[n] = timeSignal[2*n+1];
        }
        eSpec = fft(even);
        oSpec = fft(odd);
        std::complex<double> W(0,-2.0*std::acos(-1)/N); // W = -j*2*pi/N
        for (int n=0; n<N/2; ++n)
        {
            std::complex<double> factor = exp(W*static_cast<double>(n));
            spectrum[n] = eSpec[n] + factor*oSpec[n];
            spectrum[n+N/2] = eSpec[n] - factor*oSpec[n];
        }
    }

    return spectrum;

}
